package controllers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.Driver;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *Класс для использования JS скриптов
 *Для читабельности controllers.Worker'a и уменьшения строк кода в нем
 */


public class Scripts {


    public static  String selectCategory(String param){
        return "  var run=document.getElementById(\"nav-categories\");\n" +
                "        var arr=run.getElementsByTagName('a');\n" +
                "        //var categories=[];\n" +
                "        var refs;\n" +
                "        var catName='"+param+"';\n" +
                "        for(var i=0;i<arr.length;i++){\n" +
                "            if(arr[i].innerHTML===catName){\n" +
                "                refs=arr[i].href;\n" +
                "            }\n" +
                "        }\n" +
                "        return refs;";
    }

    public static String selectCat(WebDriver driver,String param){
        WebElement run=driver.findElement(By.id("nav-categories"));
        List<WebElement> arr=run.findElements(By.tagName("a"));
        String refs="";
        String catName=param;
        for(int i=0;i<arr.size();i++){
            if(arr.get(i).getAttribute("innerHTML").equals(catName)){
                refs=arr.get(i).getAttribute("href");
            }
        }
        return refs;
    }

    public  static String selectItems(String...params){
        return " var server='https://www.supremenewyork.com';\n" +
                " var run=document.getElementById(\"container\");\n" +
                "            var arr=run.getElementsByClassName(\"name-link\");\n" +
                "            var names=[];\n" +
                "            var colors=[];\n" +
                "            var refs=[];\n" +
                "            var name='"+params[0]+"';\n" +
                "            var color='"+params[1]+"';\n" +
                "            for(var i=1;i<arr.length;i+=2) {\n" +
                "                colors[i-1] = arr[i].innerHTML;\n" +
                "            }\n" +
                "            for(var j=0;j<arr.length;j+=2){\n" +
                "                names[j]=arr[j].innerHTML;\n" +
                "                refs[j]=arr[j].getAttribute('href');\n" +
                "            }\n" +
                "            for(var k=0;k<colors.length;k++){\n" +
                "                colors.splice(k+1,1);\n" +
                "                names.splice(k+1,1);\n" +
                "                refs.splice(k+1,1);\n" +
                "            }\n" +
                "            var ref;\n" +
                "            for(var a=0;a<colors.length;a++){\n" +
                "                if(colors[a]===color&&names[a]===name)\n" +
                "                    ref=refs[a];\n" +
                "            }\n" +
                "           return ref;\n";
    }

    public static String selectItem(WebDriver driver,String...params) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("container")));
        WebElement run = driver.findElement(By.id("container"));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("name-link")));
        List<WebElement> arr = run.findElements(By.className("name-link"));
        String colors[]=new String[arr.size()];
        String names[]=new String[arr.size()];
        String refs[]=new String[arr.size()];
        String name=params[0];
        String color=params[1];
        System.out.println(arr.size());
        for(int i=1;i<arr.size();i+=2){
            colors[i-1]=arr.get(i).getAttribute("innerHTML");
        }
        for(int i=0;i<arr.size();i+=2){
            names[i]=arr.get(i).getAttribute("innerHTML");
            refs[i]=arr.get(i).getAttribute("href");
        }
        ArrayList<String> colors1=new ArrayList<>(Arrays.asList(colors));
        ArrayList<String> names1=new ArrayList<>(Arrays.asList(names));
        ArrayList<String> refs1=new ArrayList<>(Arrays.asList(refs));
        for(int i=0;i<colors1.size();i++){
            colors1.remove(i+1);
            names1.remove(i+1);
            refs1.remove(i+1);
        }

        String ref="";
        for(int i=0;i<colors1.size();i++){
            if(colors1.get(i).equals(color) && names1.get(i).equals(name)){
                ref=refs1.get(i);
                break;
            }
        }
        System.out.println(colors1.size());
        return ref;

    }

    public static String selectSize(String param){
        return "var l_length = document.getElementById('size');\n" +
                "if(l_length.options==null){\n" +
                "console.log(\"no size\");\n" +
                "}\n" +
                "else{\n" +
                "for (i = 0; i < l_length.options.length; i++){\n" +
                "    x = document.getElementsByTagName(\"option\")[i].text;\n" +
                "    if (x == '"+param+"'){\n" +
                "        x = document.getElementsByTagName(\"option\")[i].value;\n" +
                "        document.getElementById('size').value = x;\n" +
                "    }\n" +
                "}\n" +
                "}";
    }

    public static void selectSize(WebDriver driver, String param){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("size")));
        WebElement l_length=driver.findElement(By.id("size"));
        System.out.println(l_length.findElements(By.xpath("./*")).get(0).getAttribute("innerHTML"));
        if(l_length.findElements(By.tagName("options"))==null){
            System.out.println("no size");
        }
        else {
            for(int i=0;i<l_length.findElements(By.xpath("./*")).size();i++){
                String x=l_length.findElements(By.xpath("./*")).get(i).getAttribute("innerHTML");
                if(x.equals(param)){
                    x=l_length.findElements(By.xpath("./*")).get(i).getAttribute("value");
                    //((ChromeDriver) driver).executeScript("document.getElementById('size').value = "+x+";");
                    driver.findElement(By.id("size")).sendKeys(x);
                }

            }
        }
    }

    public static String addToCart(){ //В итоге не стал использовать потому что клики лучше работают через API Selenium
        return "function click(el){\n" +
                "  var evt = document.createEvent(\"MouseEvents\");\n" +
                "  evt.initMouseEvent(\"click\", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);\n" +
                "  el.dispatchEvent(evt);\n" +
                "}\n" +
                "\n" +
                "function addToCart(){\n" +
                "  var run = document.getElementById(\"add-remove-buttons\");\n" +
                "  var arr=run.childNodes;\n" +
                "  return arr[0];\n" +
                "}\n" +
                "function checkout(){\n" +
                "  var run = document.getElementById(\"cart\");\n" +
                "  var arr=run.childNodes;\n" +
                "  return arr[2];\n" +
                "}\n" +
                "\n" +
                "click(addToCart());";
    }

    public static String checkOutForm(String[] params){
        return "var full_name = \""+params[0]+"\"; \n" +
                "  document.getElementById(\"order_billing_name\").value = full_name; \n" +
                "  \n" +
                "  var email = \""+params[1]+"\"; \n" +
                "  document.getElementById(\"order_email\").value = email; \n" +
                "  \n" +
                "  var tel = \""+params[2]+"\"; \n" +
                "  document.getElementById(\"order_tel\").value = tel; \n" +
                "  \n" +
                "  var address = \""+params[3]+"\"; \n" +
                "  document.getElementById(\"bo\").value = address; \n" +
                "  \n" +
                "  var city = \""+params[4]+"\"; \n" +
                "  document.getElementById(\"order_billing_city\").value = city; \n" +
                "  \n" +
                "  var postcode = \""+params[5]+"\"; \n" +
                "  document.getElementById(\"order_billing_zip\").value = postcode; \n" +
                "  \n" +
                "  var country = \""+params[6]+"\"; \n" +
                "  document.getElementById(\"order_billing_country\").value = country; \n" +
                "  \n" +
                "  var card_type = \""+params[7]+"\"; \n" +
                "  document.getElementById(\"credit_card_type\").value = card_type; \n" +
                "  \n" +
                "  var card_number = \""+params[8]+"\"; \n" +
                "  document.getElementById(\"cnb\").value = card_number; \n" +
                "  \n" +
                "  var month = \""+params[9]+"\"; \n" +
                "  document.getElementById(\"credit_card_month\").value = month; \n" +
                "  \n" +
                "  var year = \""+params[10]+"\"; \n" +
                "  document.getElementById(\"credit_card_year\").value = year; \n" +
                "  \n" +
                "  var cvv = \""+params[11]+"\"; \n" +
                "  document.getElementById(\"vval\").value = cvv; ";
    }

    public static void CheckOutForm(String[] params, ChromeDriver driver)  {

        String[] IDs={"order_billing_name","order_email","order_tel","bo","order_billing_city","order_billing_zip",
                      "order_billing_country","credit_card_type","cnb","credit_card_month","credit_card_year","vval"};
        for(int i=0;i<IDs.length;i++) {
            driver.findElement(By.id(IDs[i])).sendKeys(params[i]);
        }

    }



}

