package controllers;

import org.openqa.selenium.By;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import sun.security.krb5.internal.crypto.Des;

import java.util.Collections;
import java.util.List;

public class Worker extends Thread {

    private final String server="https://www.supremenewyork.com";
    private int itemsCount;
    private long timeStart,timeFinish;
    private String[] colors;
    private String[] names;
    private String[] categories;
    private String[] sizes;
    private String proxy;
    private String path_to_chrome;

    private String[] user_info;
    public Worker(int itemsCount, String[] colors, String[] names, String[] categories, String[] sizes, String proxy,
                  String[] user_info){//,String path_to_chrome){
        this.itemsCount=itemsCount;
        this.colors=colors;
        this.names=names;
        this.categories=categories;
        this.sizes=sizes;
        this.proxy=proxy;
        this.user_info=user_info;
        //this.path_to_chrome=path_to_chrome;
        run();
    }
    Worker(int itemsCount){
        this.itemsCount=itemsCount;
    }

    @Override
    public void run() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Xiaomi\\Desktop\\chromedriver.exe");
       ChromeOptions option = new ChromeOptions();
        if(!proxy.equals("")) {
             option.addArguments("--proxy-server="+proxy);
        }

       option.setExperimentalOption("useAutomationExtension", false);
        option.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
        option.addArguments("--fast-start");
        option.addArguments("--force-first-run");
        option.setPageLoadStrategy(PageLoadStrategy.NONE);
        ChromeDriver driver = new ChromeDriver(option);

        driver.get("https://accounts.google.com/signin/v2/identifier?hl=ru&passive=true&continue=https%3A%2F%2Fwww.google.ru%2F&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.id("identifierId")).sendKeys("nikkraew22@gmail.com");
        driver.findElement(By.id("identifierNext")).click();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath("//*[@id=\"password\"]/div[1]/div/div[1]/input")).sendKeys("WoT_zadrot12");
        driver.findElement(By.id("passwordNext")).click();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        DesiredCapabilities caps = new DesiredCapabilities();
//        caps.setCapability(CapabilityType.PAGE_LOAD_STRATEGY,"eager");
        //DesiredCapabilities caps= new DesiredCapabilities();
        //caps.setCapability("firefox_binary","C:\\Users\\Xiaomi\\Desktop\\geckodriver.exe");
        //System.setProperty("webdriver.gecko.driver","C:\\Users\\Xiaomi\\Desktop\\geckodriver.exe");
        //FirefoxDriver driver= new FirefoxDriver(caps);
        for(int i=0;i<itemsCount;i++) {
            System.out.println("https://www.supremenewyork.com/shop/all/"+categories[i].toLowerCase());
            driver.get("https://www.supremenewyork.com/shop/all/"+categories[i].toLowerCase());
            if(i==0)
                this.timeStart=System.currentTimeMillis();
             String ref;
            ref=Scripts.selectItem(driver,names[i],colors[i]);
            System.out.println(ref);
            driver.navigate().to(ref);
            if(!sizes[i].equals(" ")) {
                Scripts.selectSize(driver,sizes[i]);
            }
            WebDriverWait wait = new WebDriverWait(driver, 10);
            WebElement run=driver.findElement(By.id("add-remove-buttons"));
            List<WebElement> arr=run.findElements(By.xpath("./*"));

            wait.until(ExpectedConditions.elementToBeClickable(arr.get(0))).click();
            wait.until(ExpectedConditions.elementToBeClickable(By.id("cart-remove")));

        }
        WebElement checkout_btn=driver.findElement(By.id("cart")).findElements(By.xpath("./*")).get(2);
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(checkout_btn)).click();
        System.out.println(driver.getCurrentUrl());
        try {
            Thread.sleep(320);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
       // Scripts.CheckOutForm(user_info,driver);
        //5469380067245935


        Scripts.CheckOutForm(user_info,driver);
        //Scripts.checkOutForm(user_info);
        WebElement chekBox=driver.findElements(By.className("icheckbox_minimal")).get(1);
        chekBox.click();
        WebElement arr1=driver.findElement(By.id("pay"));
        arr1.findElements(By.xpath("./*")).get(1).click();
        this.timeFinish=System.currentTimeMillis();
        System.out.println("Время работы потока "+Thread.currentThread().getName()+" :"+(this.timeFinish-this.timeStart)/1000);
    }
}

