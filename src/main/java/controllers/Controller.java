package controllers;

import java.awt.event.ActionEvent;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;


public class Controller {
    @FXML
    private ImageView settings_btn, location_btn, exit_btn,start,up,up1,up2,cvv;

    @FXML
    private JFXTextField size,color,proxy;

    @FXML
    private AnchorPane stage;
    @FXML
    private JFXTextField user_name,user_phone,user_adress,user_city,user_email,user_postcode,user_cardNumber,user_cvv;
    @FXML
    private AnchorPane h_settings, h_location;

    @FXML
    private JFXComboBox<String> categories;


    @FXML
    private JFXComboBox<String> user_country,user_paymentSyst,user_date_day,user_date_year;

    @FXML
    private JFXButton testProxy,pickChrome;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private AnchorPane leftbar;

    private File file;
    private final int k=12;
    private String[] user_info;



    @FXML
    void initialize() {
        categories.getItems().addAll("Jackets","Shirts","Pants","etc");
        user_country.getItems().addAll("RU","BY");
        user_paymentSyst.getItems().addAll("visa","american_express","master","solo","paypal");
        for(int i=1;i<13;i++){
            if(i<10)
                user_date_day.getItems().add("0"+String.valueOf(i));
            else
                user_date_day.getItems().add(String.valueOf(i));
        }
        for(int i=2019;i<2029;i++){
            user_date_year.getItems().add(String.valueOf(i));
        }
        loadPayment();
    }

    @FXML
    void settings_click(){
        System.out.println("SETTINGS");
        h_settings.setVisible(true);
        h_location.setVisible(false);
    }
    @FXML
    void exit_click(){
        safePayment();
        Platform.exit();
    }
    @FXML
    void location_click(){
        h_location.setVisible(true);
        h_settings.setVisible(false);
        System.out.println("LOCATION");
    }


    void SafeProfile(String []user_info,String profileName){
        Profiles profile = new Profiles(user_info,profileName);
        profile.saveProfile();
    }

    String [] LoadProfile(String []user_info,String profileName){
        Profiles profile = new Profiles(user_info,profileName);
        return profile.loadProfile();
    }
    void DeleteProfile(String []user_info,String profileName){
        Profiles profile = new Profiles(user_info,profileName);
        profile.deleteProfile();
    }

    void loadPayment(){
        SaveSettings save = new SaveSettings(0);
        for(int i=0;i<k;i++){
            System.out.println(save.getString(String.valueOf(i)));
        }
    }


    void safePayment(){
        SaveSettings save = new SaveSettings(0);
        for(int i=0;i<k;i++){
            save.putString(String.valueOf(i),user_info[i]);
        }
    }

    @FXML
    void startBot() {
        user_date_day.setValue("12");
          user_info=new String[]{user_name.getText(),user_email.getText(),user_phone.getText(),
                user_adress.getText(),user_city.getText(),user_postcode.getText(),
                user_country.getValue(),user_paymentSyst.getValue(),
                user_cardNumber.getText(),user_date_day.getValue(),
                user_date_year.getValue(),user_cvv.getText()};
        System.out.println("Start");


        for(int i=0;i<1;i++) {
            new Worker(1, new String[]{color.getText(),"Lime","Grey"}, new String[]{"Zip Car Jacket","Plaid Flannel Shirt","Piping Track Jacket "},
                    new String[]{categories.getValue(),"shirts","jackets"},
                    new String[]{size.getText(),"Medium","Medium"}, proxy.getText(),user_info);//,file.getAbsolutePath());
        }
//        try {
//            new testCaptcha();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

    }

    @FXML
    void testProxy(){
        new TestProxy(proxy.getText());
    }
    
    @FXML
    void setPickChrome(){
        FileChooser fileChooser = new FileChooser();

        // Set extension filter
        FileChooser.ExtensionFilter extFilter =
                new FileChooser.ExtensionFilter("Exe files (*.exe)", "*.exe");
        fileChooser.getExtensionFilters().add(extFilter);

        // Show open file dialog
        file = fileChooser.showOpenDialog(stage.getScene().getWindow());
    }


}
