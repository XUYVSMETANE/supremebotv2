package controllers;

import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

public class Profiles {

    private String [] user_info;
    private Preferences prefs;
    private String profileName;
    Profiles(String [] user_info,String profileName){
        prefs = Preferences.userRoot().node(this.getClass().getName());
        this.user_info=user_info;
        this.profileName=profileName;
    }


    public void saveProfile(){
        for(int i=0;i<user_info.length;i++) {
            prefs.put(String.valueOf(i)+":"+profileName,user_info[i]);
        }
    }

    public String[] loadProfile(){
        String[] retArr=new String[user_info.length];
        for(int i=0;i<user_info.length;i++){
            retArr[i]=prefs.get(String.valueOf(i)+":"+profileName,null);
        }
        return retArr;
    }

    public void deleteProfile(){
        for(int i=0;i<user_info.length;i++) {
            prefs.remove(String.valueOf(i)+":"+profileName);
        }
    }

    public void deleteAllProfiles() throws BackingStoreException {
        prefs.clear();
    }
}
